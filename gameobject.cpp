#include "gameobject.h"

void GameObject::loadPoints(const char *filename)
{
	points.clear();
	FILE *file = fopen(filename, "r");
	while (file && true) {
		float x, y;
		int err = fscanf(file, "{ %f , %f },\n", &x, &y);
		if (err < 2)
			break;
        Point p = { x, y };
		points.push_back(p);
	}
	if (file)
		fclose(file);
}

void GameObject::copyPoints()
{
	opoints = points;
}

void GameObject::movePoints(float x, float y)
{
	for (unsigned c = 0; c < opoints.size(); c++) {
		opoints[c][0] += x;
		opoints[c][1] += y;
	}
}

void GameObject::scalePoints(float sx, float sy)
{
	for (unsigned c = 0; c < opoints.size(); c++) {
		opoints[c][0] *= sx;
		opoints[c][1] *= sy;
	}
}

void GameObject::rotatePoints()
{
	for (unsigned c = 0; c < opoints.size(); c++) {
		float x = opoints[c][0];
		float y = opoints[c][1];
		opoints[c][0] = cosf(-rotation) * x - sinf(-rotation) * y;
		opoints[c][1] = sinf(-rotation) * x + cosf(-rotation) * y;
	}
}

void GameObject::updateCollisionBox()
{
	collx1 = colly1 = FLT_MAX;
	collx2 = colly2 = FLT_MIN;

	for (unsigned c = 0; c < opoints.size(); c++) {
		if (opoints[c][0] == INFINITY || opoints[c][1] == INFINITY) continue;
		if (opoints[c][0] < collx1) collx1 = opoints[c][0];
		if (opoints[c][1] < colly1) colly1 = opoints[c][1];
		if (opoints[c][0] > collx2) collx2 = opoints[c][0];
		if (opoints[c][1] > colly2) colly2 = opoints[c][1];
	}

	collxcenter = (collx1 + collx2) / 2;
	collycenter = (colly1 + colly2) / 2;
}

void GameObject::drawNoWrap(float deltax, float deltay)
{
	if (!active)
		return;

	copyPoints();
	rotatePoints();
	scalePoints(scale, scale);
	movePoints(posx + deltax, posy + deltay);

    if(points.empty())
        return;
	for (unsigned c = 0; c < points.size() - 1; c++) {
		if (points[c][0] == INFINITY || points[c + 1][0] == INFINITY)
			continue;
		SDL_RenderDrawLine(screen.renderer, static_cast<int>(opoints[c][0]), static_cast<int>(opoints[c][1]),
			static_cast<int>(opoints[c + 1][0]), static_cast<int>(opoints[c + 1][1]));
	}
}

void GameObject::loadSounds()
{

}

bool GameObject::doesPointCollide(float cx, float cy)
{
	if (!active)
		return false;

	for (int y = 0; y < 1; y++) {
		for (int x = 0; x < 1; x++) {

			float xw = static_cast<float>(x*screen.WIDTH);
			float yh = static_cast<float>(y*screen.HEIGHT);

			movePoints(xw, yh);
			updateCollisionBox();

			if (cx >= collx1 && cy >= colly1 && cx <= collx2 && cy <= colly2)
				return true;
		}
	}
	return false;
}

bool GameObject::doesCollideObject(GameObject object)
{
	if (!active || !object.active)
		return false;

	copyPoints();
	rotatePoints();
	movePoints(posx, posy);

	for (unsigned p = 0; p < opoints.size(); p++) {

		if (object.doesPointCollide(opoints[p][0], opoints[p][1]))
			return true;
	}

	return false;
}

void GameObject::draw(float deltax, float deltay)
{
	drawNoWrap(deltax, deltay);

	updateCollisionBox();

	if (doesWrapX) {
		if (collx1 < 0)
			drawNoWrap(deltax + screen.WIDTH, deltay);
		if (collx2 >= screen.WIDTH)
			drawNoWrap(deltax - screen.WIDTH, deltay);
	}
	if (doesWrapY) {
		if (colly1 < 0)
			drawNoWrap(deltax, deltay + screen.HEIGHT);
		if (colly2 >= screen.HEIGHT)
			drawNoWrap(deltax, deltay - screen.HEIGHT);
	}
}

void GameObject::update(float deltaTime)
{
	if (!active)
		return;

	posx += velx * deltaTime;
	posy += vely * deltaTime;
	if (doesWrapX) {
		if (posx < 0) posx += screen.WIDTH;
		if (posx >= screen.WIDTH) posx -= screen.WIDTH;
	}
	if (doesWrapY) {
		if (posy < 0) posy += screen.HEIGHT;
		if (posy >= screen.HEIGHT) posy -= screen.HEIGHT;
	}
}
