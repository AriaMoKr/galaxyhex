#pragma once

#include <cfloat>
#include <vector>
#include <cstdio>
using namespace std;

#include "screen.h"
#include "point.h"

class GameObject {
protected:
	float collx1, colly1, collx2, colly2, collxcenter, collycenter;
	bool doesWrapX = false, doesWrapY = false;
	void loadPoints(const char *filename);
	float scale = 1.0f;
	vector<Point> opoints;
	virtual void copyPoints();
	virtual void movePoints(float x, float y);
	virtual void scalePoints(float sx, float sy);
	virtual void rotatePoints();
	void updateCollisionBox();
	void drawNoWrap(float deltax, float deltay);
	virtual void loadSounds();
public:
	enum Size { large, medium, small };
	unsigned scoreValue = 0;
	float posx = 0.0f, posy = 0.0f;
	float velx = 0.0f, vely = 0.0f;
	bool active = false;
    float rotation = 0.0f;
	vector<Point> points;
	int explosionSound = -1, shootSound = -1, thrusterSound = -1;
	bool doesPointCollide(float cx, float cy);
	bool doesCollideObject(GameObject object);
	void draw(float deltax, float deltay);
	virtual void update(float deltaTime);
};
