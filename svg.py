from __future__ import print_function
from xml.dom import minidom
from svg.path import parse_path

def loadLayer(layer, layerFilename):
    path_strings = [path.getAttribute('d') for path in layer.getElementsByTagName('path')]

    file = open(layerFilename, 'w')

    for path in path_strings:
        first = None
        last = None
        lines = parse_path(path)
        for line in lines:
            for part in range(2):
                p = line.point(part)
                if first == None:
                    first = p
                last = p
                file.write('{{ {0} , {1} }},\n'.format(p.real, p.imag))
        if lines.closed:
            file.write('{{ {0} , {1} }},\n'.format(first.real, first.imag))
        file.write('{ INFINITY , INFINITY },\n')

doc = minidom.parse('assets/graphics.svg')

for svg in doc.getElementsByTagName('svg'):
    for g in doc.getElementsByTagName('g'):
        layer = g.attributes['inkscape:label'].value
        if layer == 'grid':
            continue
        loadLayer(g, 'assets/' + layer + '.txt')

doc.unlink()
