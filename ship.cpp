#include "ship.h"

Ship::Ship()
{
	loadPoints("assets/ship.txt");

	active = true;
	doesWrapX = doesWrapY = true;

}

void Ship::loadSounds()
{
	explosionSound = audio.getSoundResource("assets/ship_explosion.wav");
	shootSound = audio.getSoundResource("assets/ship_shoot.wav");
	thrusterSound = audio.getSoundResource("assets/ship_thruster.wav");
}

void Ship::drawIcon(float x, float y)
{
	copyPoints();
	movePoints(x, y);
    if(opoints.empty())
        return;
	for (unsigned c = 0; c < opoints.size() - 1; c++) {
		if (points[c][0] == INFINITY || points[c + 1][0] == INFINITY)
			continue;
		SDL_RenderDrawLine(screen.renderer, static_cast<int>(opoints[c][0]), static_cast<int>(opoints[c][1]),
			static_cast<int>(opoints[c + 1][0]), static_cast<int>(opoints[c + 1][1]));
	}
}

void Ship::draw(float deltax, float deltay)
{
	GameObject::draw(deltax, deltay);

	for (unsigned c = 0; c < maxTorpedos; c++)
		torpedos[c].draw(static_cast<float>(deltax), static_cast<float>(deltay));

	for (auto &e : explosion_pieces) {
		e.draw(deltax, deltay);
	}
}

void Ship::shoot()
{
	if (!active)
		return;

	for (unsigned c = 0; c < maxTorpedos; c++)
	{
		if (!torpedos[c].active) {
			torpedos[c].shoot(opoints[0][0], opoints[0][1], (float)(rotation + M_PI / 2.0f));
			audio.play(shootSound);
			break;
		}
	}
}

void Ship::update(float deltaTime)
{
	GameObject::update(deltaTime);

	for (unsigned c = 0; c < maxTorpedos; c++)
		torpedos[c].update(deltaTime);

	for (auto &e : explosion_pieces) {
		e.update(deltaTime);
	}

	if (restartTimer > 0) {
		restartTimer -= deltaTime;
	}
}

void Ship::reset()
{
	if (active || restartTimer > 0)
		return;

	active = true;
	posx = screen.WIDTH / 2.0f;
	posy = screen.HEIGHT / 2.0f;
	velx = 0;
	vely = 0;
	rotation = 0;

	for (auto &torpedo : torpedos)
		torpedo.active = false;

	explosion_pieces.clear();
	explosion = false;
}

void Ship::accelerate(float time_delta)
{
	if (!active)
		return;

	velx += -sinf(rotation) * movementStartSpeed * time_delta;
	if (velx < -maxSpeed) velx = -maxSpeed;
	if (velx > maxSpeed) velx = maxSpeed;

	vely += -cosf(rotation) * movementStartSpeed * time_delta;
	if (vely < -maxSpeed) vely = -maxSpeed;
	if (vely > maxSpeed) vely = maxSpeed;

	audio.play(thrusterSound, false);
}

void Ship::warp()
{
	if (!active)
		return;

	posx = (float)(rand() % screen.WIDTH);
	posy = (float)(rand() % screen.HEIGHT);
}

void Ship::explode()
{
	if (explosion)
		return;

	for (unsigned p = 0; p < points.size() - 1; p++) {
		GameObject piece;

		piece.points.push_back(points[p]);
		piece.points.push_back(points[p + 1]);

		piece.active = true;
		piece.posx = posx;
		piece.posy = posy;

		float range = 0.02f + fabs(velx) * 0.1f;
		piece.velx = -range + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (range * 2.0f)));
		range = 0.02f + fabs(vely) * 0.1f;
		piece.vely = -range + static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / (range * 2.0f)));

		piece.rotation = rotation;
		explosion_pieces.push_back(piece);
	}

	explosion = true;
}
