#pragma once

#include <vector>
using namespace std;

#include "gameobject.h"

class Font : public GameObject {
private:
	vector<vector<Point> > characters;
	float charWidth = 40.00001f;
	float charHeight = 51.0f;
public:
	Font();
	void generateCharacters();
	void putText(float x, float y, const char *str);
};
