#include "asteroid.h"
#include <string>
using namespace std;

Asteroid::Asteroid() {
	int num = (rand() % 3) + 1;
	string filename = "assets/asteroid" + to_string(num) + ".txt";
	loadPoints(filename.c_str());
	active = true;
	doesWrapX = doesWrapY = true;
}

void Asteroid::loadSounds()
{
	explosionSound = audio.getSoundResource("assets/asteroid_explosion.wav");
}

void Asteroid::update(float deltaTime)
{
	if (!active) return;

	switch (size) {
	case large:
		scale = 1.0f;
		scoreValue = 20;
		break;
	case medium:
		scale = 0.7f;
		scoreValue = 50;
		break;
	case small:
	default:
		scale = 0.3f;
		scoreValue = 100;
	}

	GameObject::update(deltaTime);
}
