#pragma once

#include <cmath>
using namespace std;

class Point
{
public:
    float x, y;
    static Point vectorToTuple(float angle, float velocity)
    {
        Point p;
        p.x = velocity * cosf(angle);
        p.y = velocity * -sinf(angle);
        return p;
    }
	float& operator[] (unsigned i) {
		return i == 1 ? y : x;
	}
};
