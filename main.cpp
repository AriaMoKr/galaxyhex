#include "game.h"

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#include <emscripten/html5.h>
#endif

#include "screen.h"
#include "audio.h"

Screen screen;
Audio audio;
Game game;

void mainloop()
{
	game.mainloop();
}

int main(int, char**)
{
	printf("starting...\n");

	if (game.premainloop() != 0)
		return 1;

#ifdef __EMSCRIPTEN__
	emscripten_set_main_loop(mainloop, 0, 1);
#else
	while (!game.quit) {
		mainloop();
	}
#endif

	printf("exiting...\n");
	game.postmainloop();

	return 0;
}
