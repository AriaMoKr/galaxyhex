#pragma once

#ifdef _WINDOWS
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include <cmath>
#include <vector>
using namespace std;

#include "screen.h"
#include "gameobject.h"
#include "torpedo.h"
#include "asteroid.h"

class Ship : public GameObject {
private:
public:
	float rotationMaxSpeed = 0.004f;
	float movementStartSpeed = 0.0005f;
	const static unsigned maxTorpedos = 4;
	Torpedo torpedos[maxTorpedos];
	unsigned lastAccel = 0;
	unsigned accelWait = 0;
    const float restartTimerStart = 2000.0f;
    float restartTimer = 0.0f;
	float maxSpeed = 1.0f;
	vector<GameObject> explosion_pieces;
	bool explosion = false;
	Ship();
	void loadSounds();
	void drawIcon(float x, float y);
	void draw(float deltax, float deltay);
	void shoot();
	void update(float deltaTime);
	void reset();
	void accelerate(float time_delta);
    void warp();
	void explode();
};
