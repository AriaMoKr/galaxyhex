#pragma once

#ifdef _WINDOWS
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include <cfloat>
using namespace std;

#include "screen.h"
#include "gameobject.h"
#include "torpedo.h"

class UFO : public GameObject {
private:
	Size size = small;
	float initialShootDelay = 500.0f;
	float shootDelay = initialShootDelay;
	float initialMoveDelay = 1000.0f;
	float moveDelay = initialMoveDelay;
public:
	const static unsigned maxTorpedos = 2;
	Torpedo torpedos[maxTorpedos];
	UFO();
	void loadSounds();
	void draw(float deltax, float deltay);
	void shoot();
	void reset();
	void move();
	void update(float deltaTime);
};
