#pragma once

#ifndef __ANDROID__
#define USE_SOUND
#endif

#ifdef USE_SOUND
#ifdef _WINDOWS
#include <SDL_mixer.h>
#elif __APPLE__
#include <SDL2_mixer/SDL_mixer.h>
#else
#include <SDL2/SDL_mixer.h>
#endif
#endif

#include <map>
#include <string>
using namespace std;

struct Sound_Info
{
#ifdef USE_SOUND
	Mix_Chunk *sound;
#endif
	int channel;
	bool playing;
};

class Audio
{
private:
	map<int, Sound_Info> sounds;
	map<string, int> sound_map;
	
	int loadSound(const char *filename);
public:
	int getSoundResource(const char *filename);
	int init();
	int play(int resource, bool multiple = true);
	void shutdown();
};

extern Audio audio;
