#pragma once

#ifdef _WINDOWS
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include <cfloat>
#include <iostream>
#include <fstream>
using namespace std;

#include "audio.h"
#include "screen.h"
#include "gameobject.h"

class Asteroid : public GameObject {
public:
	Size size = large;
	Asteroid();
	void loadSounds();
	void update(float deltaTime);
};
