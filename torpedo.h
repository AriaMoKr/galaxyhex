#pragma once

#ifdef _WINDOWS
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#include "screen.h"
#include "gameobject.h"

class Torpedo : public GameObject {
public:
	float distancex, distancey;
	float movementStartSpeed = 0.5f;
    bool doesWrap = true;
	void shoot(float _posx, float _posy, float _rotation);
	void update(float deltaTime);
	Torpedo();
};
