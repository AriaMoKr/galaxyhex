#pragma once

#include <iostream>
using namespace std;

#ifdef _WINDOWS
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

class Screen {
public:
	const int WIDTH = 640, HEIGHT = 480;
	SDL_Window *window = NULL;
	SDL_Renderer *renderer = NULL;
	SDL_Joystick *joy;
	const Uint8 *state;
	unsigned int currentTime = SDL_GetTicks();

	void logSDLError(const char *msg) {
		cerr << msg << " error: " << SDL_GetError() << endl;
	}

	int init()
	{
		if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_EVENTS | \
			SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER) != 0) {
			logSDLError("SDL_Init");
			return 1;
		}

		if (SDL_CreateWindowAndRenderer(WIDTH, HEIGHT, 0, &window, &renderer) != 0) {
			logSDLError("SDL_CreateWindowAndRenderer");
			SDL_Quit();
			return 1;
		}
		
		if (SDL_NumJoysticks() > 0)
			joy = SDL_JoystickOpen(0);

		return 0;
	}
};

extern Screen screen;
