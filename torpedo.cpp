#include "torpedo.h"

void Torpedo::shoot(float _posx, float _posy, float _rotation) {
	active = true;
	this->posx = _posx;
	this->posy = _posy;
	distancex = distancey = 0.0f;
	velx = cosf(_rotation) * movementStartSpeed;
	vely = -sinf(_rotation) * movementStartSpeed;
}

void Torpedo::update(float deltaTime) {
	if (!active) return;

	distancex += fabs(velx * deltaTime);
	distancey += fabs(vely * deltaTime);

	if (distancex > (screen.WIDTH * 0.6f)) active = false;
	if (distancey > (screen.HEIGHT * 0.6f)) active = false;

	GameObject::update(deltaTime);
}

Torpedo::Torpedo()
{
	loadPoints("assets/torpedo.txt");
	doesWrapX = doesWrapY = true;
}
