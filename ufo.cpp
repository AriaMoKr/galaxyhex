#include "ufo.h"
#include "audio.h"

UFO::UFO()
{
	loadPoints("assets/ufo.txt");
	active = true;
	doesWrapX = false;
	doesWrapY = true;
}

void UFO::loadSounds()
{
	explosionSound = audio.getSoundResource("assets/ufo_explosion.wav");
	shootSound = audio.getSoundResource("assets/ship_shoot.wav");
	//TODO: add ufo movement sound
}

void UFO::draw(float deltax, float deltay)
{
	GameObject::draw(deltax, deltay);

	for (unsigned c = 0; c < maxTorpedos; c++)
		torpedos[c].draw(static_cast<float>(deltax), static_cast<float>(deltay));
}

void UFO::shoot()
{
	if (!active)
		return;

	updateCollisionBox();

	for (unsigned c = 0; c < maxTorpedos; c++)
	{
		if (!torpedos[c].active) {
			float angle = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (2 * M_PI)));
			torpedos[c].shoot(collxcenter, collycenter, angle);
			audio.play(shootSound);
			break;
		}
	}
}

void UFO::reset()
{
	vely = 0.0f;
	for (auto &torpedo : torpedos)
		torpedo.active = false;
	shootDelay = initialShootDelay;
	moveDelay = initialMoveDelay;
}

void UFO::move()
{
	if (fabs(vely) > 0.01f)
		vely = 0.0f;
	else
		vely = rand() % 2 ? 0.1f : -0.1f;
}

void UFO::update(float deltaTime)
{
	switch (size) {
	case large:
		scale = 0.75f;
		scoreValue = 200;
		break;
	case small:
	default:
		scale = 0.4f;
		scoreValue = 1000;
	}

	GameObject::update(deltaTime);

	for (auto &t : torpedos)
		t.update(deltaTime);

	shootDelay -= deltaTime;
	if (shootDelay <= 0.0f) {
		shoot();
		shootDelay = initialShootDelay;
	}

	moveDelay -= deltaTime;
	if (moveDelay <= 0.0f) {
		move();
		moveDelay = initialMoveDelay;
	}

	if (posx < 0)
		active = false;

	if (posx > screen.WIDTH)
		active = false;

}
