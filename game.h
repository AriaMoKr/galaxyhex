#pragma once

#ifdef _WINDOWS
#include <SDL.h>
#else
#include <SDL2/SDL.h>
#endif

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#include <vector>
#include <sstream>
using namespace std;

#include "screen.h"
#include "audio.h"
#include "ship.h"
#include "asteroid.h"
#include "font.h"
#include "ufo.h"
#include "point.h"

class Game {
public:
	bool quit = false;
private:
    int lives;
	Ship ship;
    UFO ufo;
	vector<Asteroid> asteroids;
	Font font;
	unsigned timeDelta, lastTime, score = 0, bestScore = 0, level;
	bool pause = false;
	int music_duh = -1, music_dum = -1;
    const float playZone = 0.01f; //outer border to create asteroids
	const float safetyZone = 0.35f; //inner section to wait for clear before restart
	const float ufoTimerStart = 10000.0f;
	float ufoTimer = 0.0f;

	void processInput();
	void splitAsteroid(Asteroid &asteroid, vector<Asteroid> &new_asteroids);
	bool restartIsSafe();
	void update(float fTimeDelta);
	bool pointInMiddleSection(float x, float y, float zone);
	Point newPositionNotInMiddle();
	void placeUFO();
	void levelReset();
	void reset();
	void playerDeath();
	void drawLives();
	void render();
	void playMusic();
public:
	int premainloop();
	void mainloop();
	void postmainloop();
};
