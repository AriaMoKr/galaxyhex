#include "font.h"

Font::Font() {
	loadPoints("assets/font.txt");

	active = true;

	generateCharacters();
}

void Font::generateCharacters()
{
	bool closedPath = true;
	characters.clear();
	for (int iY = 0; iY < 4; iY++)
	{
		for (int iX = 0; iX < 10; iX++)
		{
			vector<Point> charPoints;

			for (unsigned c = 0; c < points.size(); c++)
			{
				if (points[c][0] == INFINITY && points[c][1] == INFINITY)
				{
					if (closedPath) continue;
					closedPath = true;
				}
				else
				{
					if (points[c][0] < (charWidth*iX) || points[c][0] >= (charWidth*(iX + 1)))
						continue;
					if (points[c][1] < (charHeight*iY) || points[c][1] >= (charHeight*(iY + 1)))
						continue;

					closedPath = false;
				}
				Point p = { points[c][0], points[c][1] };
				p.x -= (iX*charWidth) + 10.0f;
				p.y -= (iY*charHeight) + 10.0f;
				charPoints.push_back(p);
			}
			characters.push_back(charPoints);
		}
	}
}

void Font::putText(float x, float y, const char *str)
{
	float curx = x;
	for (unsigned i = 0; i < strlen(str); i++) {
		int iX, iY;
		int ch = tolower(str[i]);
		if (ch >= '0' && ch <= '9') {
			iX = ch - '0'; iY = 0;
		}
		else if (ch >= 'a' && ch <= 'z') {
			iX = ch - 'a';
			iY = 1;
		}
		else {
			iX = 9; iY = 3;
		}

		vector<Point> charPoints = characters[iY * 10 + iX];
		scale = 0.5f;
		for (unsigned c = 0; c < charPoints.size(); c++) {
			if (charPoints[c].x == INFINITY || charPoints[c + 1].y == INFINITY)
				continue;
			SDL_RenderDrawLine(screen.renderer,
				static_cast<int>(charPoints[c].x * scale + curx),
				static_cast<int>(charPoints[c].y * scale + y),
				static_cast<int>(charPoints[c + 1].x * scale + curx),
				static_cast<int>(charPoints[c + 1].y * scale + y));
		}
		curx += charWidth - 20.0f;
	}
}
