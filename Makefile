CXXFLAGS = -Wall -Wno-c++11-extensions
LDLIBS = -lSDL2 -lSDL2_mixer
OBJS = asteroid.o font.o gameobject.o ship.o torpedo.o audio.o game.o main.o startscreen.o ufo.o
CC = g++
CXX = g++
all: main

main: $(OBJS)

clean:
	rm -f main $(OBJS)
