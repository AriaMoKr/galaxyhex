#include "game.h"

void Game::processInput()
{
	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT)
			quit = true;
		else if (e.type == SDL_KEYDOWN) {
			switch (e.key.keysym.sym) {
			case SDLK_LCTRL:
			case SDLK_z:
				if (!e.key.repeat && !pause)
					ship.shoot();
				break;
			case SDLK_r:
				reset();
				break;
			case SDLK_s:
				playerDeath();
				break;
			case SDLK_f:
				SDL_SetWindowFullscreen(screen.window, SDL_WINDOW_FULLSCREEN);
				break;
			case SDLK_w:
				SDL_SetWindowFullscreen(screen.window, 0);
				break;
			case SDLK_ESCAPE:
				quit = true;
				break;
			case SDLK_p:
				if (!e.key.repeat)
					pause = !pause;
				break;
			case SDLK_SPACE:
				if (!e.key.repeat && !pause)
					ship.warp();
				break;
			}
		}
		else if (e.type == SDL_JOYBUTTONDOWN) {
			switch (e.jbutton.button) {
			case 1:
				if (!pause)
					ship.shoot();
				break;
			case 2:
				if (!e.key.repeat && !pause)
					ship.warp();
				break;
			case 11:
			case 4:
				reset();
				break;
			case 6:
				quit = true;
				break;
			case 7:
				pause = !pause;
				break;
			}
		}
	}

	float fTimeDelta = static_cast<float>(timeDelta);

	SDL_PumpEvents();
	screen.state = SDL_GetKeyboardState(NULL);

	if (pause)
		return;

	if (screen.state[SDL_SCANCODE_LEFT] || SDL_JoystickGetHat(screen.joy, 0) & SDL_HAT_LEFT)
		ship.rotation += ship.rotationMaxSpeed * fTimeDelta;
	if (screen.state[SDL_SCANCODE_RIGHT] || SDL_JoystickGetHat(screen.joy, 0) & SDL_HAT_RIGHT)
		ship.rotation -= ship.rotationMaxSpeed * fTimeDelta;

	if (screen.state[SDL_SCANCODE_LALT] ||
		screen.state[SDL_SCANCODE_RALT] ||
		screen.state[SDL_SCANCODE_LSHIFT] ||
		screen.state[SDL_SCANCODE_RSHIFT] ||
		screen.state[SDL_SCANCODE_X] ||
		SDL_JoystickGetButton(screen.joy, 0))
	{
		if (screen.currentTime - ship.lastAccel > ship.accelWait) {
			ship.accelerate(fTimeDelta);


			ship.lastAccel = screen.currentTime;
		}
	}
}

void Game::splitAsteroid(Asteroid &asteroid, vector<Asteroid> &new_asteroids)
{
	asteroid.active = false;
	audio.play(asteroid.explosionSound);
	score += asteroid.scoreValue;

	if (asteroid.size == Asteroid::small)
		return;

	GameObject::Size new_size = asteroid.size == Asteroid::large ? Asteroid::medium : Asteroid::small;

	for (int a = 0; a < 2; a++) {
		Asteroid a1;
		a1.loadSounds();
		a1.posx = asteroid.posx;
		a1.posy = asteroid.posy;
		a1.size = new_size;
		float angle = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / 360.0f));
		float vel = 0.1f;
		a1.velx = vel * cosf(angle);
		a1.vely = vel * sinf(angle);
		new_asteroids.push_back(a1);
	}
}

bool Game::restartIsSafe()
{
	for (auto &a : asteroids) {
		if (a.active && pointInMiddleSection(a.posx, a.posy, safetyZone))
			return false;
	}

	return true;
}

void Game::update(float fTimeDelta)
{
	if (pause)
		return;

	ship.update(fTimeDelta);
	ufo.update(fTimeDelta);
	for (auto &asteroid : asteroids)
		asteroid.update(fTimeDelta);

	for (unsigned c = 0; c < ship.maxTorpedos; c++) {
		Torpedo &torpedo = ship.torpedos[c];
		if (!torpedo.active)
			continue;
		vector<Asteroid> new_asteroids;

		for (auto &asteroid : asteroids) {
			if (!asteroid.active || !torpedo.active)
				continue;

			if (asteroid.doesPointCollide(torpedo.posx, torpedo.posy)) {
				splitAsteroid(asteroid, new_asteroids);
				torpedo.active = false;
			}
		}
		for (auto &asteroid : new_asteroids)
			asteroids.push_back(asteroid);

		if (torpedo.active && ufo.doesPointCollide(torpedo.posx, torpedo.posy)) {
			ufo.active = false;
			audio.play(ufo.explosionSound);
			score += ufo.scoreValue;
			torpedo.active = false;
		}
	}

	for (unsigned c = 0; c < ufo.maxTorpedos; c++) {
		Torpedo &torpedo = ufo.torpedos[c];
		if (!torpedo.active)
			continue;
		vector<Asteroid> new_asteroids;

		for (auto &asteroid : asteroids) {
			if (!asteroid.active || !torpedo.active)
				continue;

			if (asteroid.doesPointCollide(torpedo.posx, torpedo.posy)) {
				splitAsteroid(asteroid, new_asteroids);
				torpedo.active = false;
			}
		}
		for (auto &asteroid : new_asteroids)
			asteroids.push_back(asteroid);

		if (torpedo.active && ship.doesPointCollide(torpedo.posx, torpedo.posy)) {
			torpedo.active = false;
			playerDeath();
			audio.play(ship.explosionSound);
		}

	}


	if (!ship.active) {
		if (lives > 0)
			if (restartIsSafe())
				ship.reset();
		return;
	}

	if (!ufo.active) {
		ufoTimer -= timeDelta;
		if (ufoTimer < 0)
			placeUFO();
	}

	vector<Asteroid> new_asteroids;
	for (auto &asteroid : asteroids) {
		if (ship.doesCollideObject(asteroid)) {
			splitAsteroid(asteroid, new_asteroids);
			playerDeath();
		}
		else if (ufo.doesCollideObject(asteroid)) {
			splitAsteroid(asteroid, new_asteroids);
			ufo.active = false;
			audio.play(ufo.explosionSound);
			score += ufo.scoreValue;
		}
	}
	for (auto &new_asteroid : new_asteroids) {
		new_asteroid.loadSounds();
		asteroids.push_back(new_asteroid);
	}

	if (ship.active && ufo.active && ship.doesCollideObject(ufo)) {
		ufo.active = false;
		score += ufo.scoreValue;
		playerDeath();
		audio.play(ufo.explosionSound);
	}
}

bool Game::pointInMiddleSection(float x, float y, float zone)
{
	if (x < screen.WIDTH*zone || x >= screen.WIDTH*(1.0f - zone))
		return false;
	if (y < screen.HEIGHT*zone || y >= screen.HEIGHT*(1.0f - zone))
		return false;
	return true;
}

Point Game::newPositionNotInMiddle()
{
	Point p;
	do {
		p.x = (float)(rand() % screen.WIDTH);
		p.y = (float)(rand() % screen.HEIGHT);
	} while (pointInMiddleSection(p.x, p.y, playZone));
	return p;
}

void Game::placeUFO()
{
	ufo.posy = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / screen.WIDTH));
	if (rand() % 2) {
		ufo.posx = (float)(screen.WIDTH - 10);
		ufo.velx = -0.1f;
	}
	else {
		ufo.posx = 10.0f;
		ufo.velx = 0.1f;
	}

	//todo avoid dangerous locations

	ufo.active = true;
	ufo.reset();
	ufoTimer = ufoTimerStart;
}

void Game::levelReset()
{
	ship.active = false;

	ship.restartTimer = 0;
	ship.reset();

	ufo.active = false;
	//ufoTimer = 2000;
	ufoTimer = ufoTimerStart;
	//placeUFO();

	asteroids.clear();
	//return; //TEMP
	for (unsigned c = 0; c < (level * 2 + 2); c++) {
		Asteroid a1;
		a1.loadSounds();
		Point p = newPositionNotInMiddle();
		a1.posx = p.x;
		a1.posy = p.y;
		a1.size = Asteroid::large;
		float angle = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (2 * M_PI)));
		Point vel = Point::vectorToTuple(angle, 0.05f);
		a1.velx = vel.x;
		a1.vely = vel.y;
		asteroids.push_back(a1);
	}
}

void Game::reset()
{
	score = 0;
	lives = 3;
	level = 1;

	levelReset();
}

void Game::playerDeath()
{
	if (!ship.active)
		return;
	lives--;
	ship.active = false;
	ship.restartTimer = ship.restartTimerStart;
	ship.explode();
	audio.play(ship.explosionSound);
}

void Game::drawLives()
{
	if (lives < 1)
		return;
	for (int c = 0; c < lives; c++)
		ship.drawIcon(100.0f + c * 22.0f, 50.0f);
}

void Game::render()
{
	SDL_SetRenderDrawColor(screen.renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(screen.renderer);

	SDL_SetRenderDrawColor(screen.renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);

	drawLives();

	ship.draw(0, 0);
	for (auto &i : asteroids)
		i.draw(0, 0);

	ufo.draw(0, 0);

	if (lives < 1)
		font.putText(225, 150, "Game Over");

	if (score > bestScore)
		bestScore = score;

	stringstream scoress;
	scoress << score;
	font.putText(100, 5, scoress.str().c_str());

	stringstream bestScoress;
	bestScoress << bestScore;
	font.putText(300, 5, bestScoress.str().c_str());

	SDL_RenderPresent(screen.renderer);
}

void Game::playMusic()
{
	static unsigned lastPlayed = 0;
	static bool play_dum = false;
	unsigned lastPlayedDelta = screen.currentTime - lastPlayed;
	if (lastPlayed == 0 || lastPlayedDelta > 2000) {
		if (play_dum)
			audio.play(music_dum);
		else
			audio.play(music_duh);
		play_dum = !play_dum;
		lastPlayed = screen.currentTime;
	}
}

int Game::premainloop()
{
	srand(static_cast<unsigned>(SDL_GetTicks()));
	if (screen.init() != 0)
		return 1;

	audio.init();

	reset();

	ship.loadSounds();
	ufo.loadSounds();
	music_duh = audio.getSoundResource("assets/music_duh.wav");
	music_dum = audio.getSoundResource("assets/music_dum.wav");
	for (auto &it : asteroids)
		it.loadSounds();

	lastTime = SDL_GetTicks();
	return 0;
}

void Game::mainloop()
{
	screen.currentTime = SDL_GetTicks();
	timeDelta = screen.currentTime - lastTime;

	render();

	processInput();
	if (timeDelta > 0) {
		float fTimeDelta = static_cast<float>(timeDelta);
		update(fTimeDelta);
		playMusic();
	}

	lastTime = screen.currentTime;
}

void Game::postmainloop()
{
	audio.shutdown();

	if (screen.renderer)
		SDL_DestroyRenderer(screen.renderer);
	if (screen.window)
		SDL_DestroyWindow(screen.window);

	SDL_Quit();
}
