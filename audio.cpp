#include "audio.h"
#include <map>
#include <string>
using namespace std;

int Audio::loadSound(const char *filename)
{
#ifdef USE_SOUND
	Mix_Chunk *wave = Mix_LoadWAV(filename);
	if (wave == NULL)
		return -1;
	int i = (int)(sound_map.size() + 1);
	Sound_Info sound = { wave, -1, false };
	sounds[i] = sound;
	return i;
#else
	return -1;
#endif
}

int Audio::getSoundResource(const char *filename)
{
	auto it = sound_map.find(string(filename));
	if (it != sound_map.end()) {
		return it->second;
	}

	int resource = loadSound(filename);
	sound_map[filename] = resource;
	return resource;
}

int Audio::init()
{
#ifdef USE_SOUND
	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
		return -1;

	Mix_AllocateChannels(16);
#endif
	return 0;
}

int Audio::play(int resource, bool multiple)
{
#ifdef USE_SOUND
	if (resource < 0)
		return -2;

	if (sounds[resource].channel != -1 && Mix_Playing(sounds[resource].channel) == 0)
		sounds[resource].channel = -1;

	if (!multiple && sounds[resource].channel != -1)
		return 0;

	Mix_Chunk *wave = sounds[resource].sound;
	sounds[resource].channel = Mix_PlayChannel(-1, wave, 0);
	sounds[resource].playing = true;
	if (sounds[resource].channel == -1)
		return -1;

	return 0;
#else
	return 0;
#endif
}

void Audio::shutdown()
{
	sound_map.clear();
#ifdef USE_SOUND
	for (auto &it : sounds)
		Mix_FreeChunk(it.second.sound);
#endif
	sounds.clear();

#ifdef USE_SOUND
	Mix_CloseAudio();
#endif
}
